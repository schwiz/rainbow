// Copyright (c) 2010-14 Bifrost Entertainment AS and Tommy Nguyen
// Distributed under the MIT License.
// (See accompanying file LICENSE or copy at http://opensource.org/licenses/MIT)

#include "Graphics/Sprite.h"
#include "Lua/lua_Sprite.h"

NS_B2_LUA_BEGIN
{
	struct BodyData
	{
		float curr_r, prev_r;
		Sprite *sprite;
		b2Vec2 curr_p, prev_p;

		BodyData(const b2BodyDef &d)
		    : curr_r(d.angle), prev_r(d.angle), sprite(nullptr),
		      curr_p(d.position), prev_p(d.position) { }
	};

	int BodyDef(lua_State *L)
	{
		lua_createtable(L, 0, 16);

		luaR_rawsetfield(L, lua_pushinteger, b2_staticBody, "type");

		lua_pushliteral(L, "position");
		lua_createtable(L, 0, 2);
		luaR_rawsetfield(L, lua_pushnumber, 0.0f, "x");
		luaR_rawsetfield(L, lua_pushnumber, 0.0f, "y");
		lua_rawset(L, -3);

		luaR_rawsetfield(L, lua_pushnumber, 0.0f, "angle");

		lua_pushliteral(L, "linearVelocity");
		lua_createtable(L, 0, 2);
		luaR_rawsetfield(L, lua_pushnumber, 0.0f, "x");
		luaR_rawsetfield(L, lua_pushnumber, 0.0f, "y");
		lua_rawset(L, -3);

		luaR_rawsetfield(L, lua_pushnumber, 0.0f, "angularVelocity");
		luaR_rawsetfield(L, lua_pushnumber, 0.0f, "linearDamping");
		luaR_rawsetfield(L, lua_pushnumber, 0.0f, "angularDamping");
		luaR_rawsetfield(L, lua_pushboolean, true, "allowSleep");
		luaR_rawsetfield(L, lua_pushboolean, true, "awake");
		luaR_rawsetfield(L, lua_pushboolean, false, "fixedRotation");
		luaR_rawsetfield(L, lua_pushboolean, false, "bullet");
		luaR_rawsetfield(L, lua_pushboolean, true, "active");
		luaR_rawsetfield(L, lua_pushnumber, 1.0f, "gravityScale");

		return 1;
	}

	void parse_BodyDef(lua_State *L, b2BodyDef &def)
	{
		const char type[] = "BodyDef";
		static_cast<void>(type);

		luaR_rawgetfield(L, "type", type);
		def.type = static_cast<b2BodyType>(luaR_tointeger(L, -1));
		lua_pop(L, 1);

		luaR_rawgetfield(L, "position", type);
		lua_pushliteral(L, "x");
		lua_rawget(L, -2);
		def.position.x = luaR_tonumber(L, -1) / ptm_ratio;
		lua_pushliteral(L, "y");
		lua_rawget(L, -3);
		def.position.y = luaR_tonumber(L, -1) / ptm_ratio;
		lua_pop(L, 3);

		luaR_rawgetfield(L, "angle", type);
		def.angle = luaR_tonumber(L, -1);
		lua_pop(L, 1);

		luaR_rawgetfield(L, "linearVelocity", type);
		lua_pushliteral(L, "x");
		lua_rawget(L, -2);
		def.linearVelocity.x = luaR_tonumber(L, -1);
		lua_pushliteral(L, "y");
		lua_rawget(L, -3);
		def.linearVelocity.y = luaR_tonumber(L, -1);
		lua_pop(L, 3);

		luaR_rawgetfield(L, "linearDamping", type);
		def.linearDamping = luaR_tonumber(L, -1);
		lua_pop(L, 1);

		luaR_rawgetfield(L, "angularDamping", type);
		def.angularDamping = luaR_tonumber(L, -1);
		lua_pop(L, 1);

		luaR_rawgetfield(L, "allowSleep", type);
		def.allowSleep = lua_toboolean(L, -1);
		lua_pop(L, 1);

		luaR_rawgetfield(L, "awake", type);
		def.awake = lua_toboolean(L, -1);
		lua_pop(L, 1);

		luaR_rawgetfield(L, "fixedRotation", type);
		def.fixedRotation = lua_toboolean(L, -1);
		lua_pop(L, 1);

		luaR_rawgetfield(L, "bullet", type);
		def.bullet = lua_toboolean(L, -1);
		lua_pop(L, 1);

		luaR_rawgetfield(L, "active", type);
		def.active = lua_toboolean(L, -1);
		lua_pop(L, 1);

		luaR_rawgetfield(L, "gravityScale", type);
		def.gravityScale = luaR_tonumber(L, -1);
		lua_pop(L, 1);
	}

	class Body : public Bind<Body>
	{
		friend Bind;

	public:
		Body(lua_State *);
		inline b2Body* get();

	private:
		static int bind(lua_State *);
		static int scale_and_position(lua_State *);

		static int create_fixture(lua_State *);
		static int destroy_fixture(lua_State *);

		static int set_transform(lua_State *);
		static int get_position(lua_State *);
		static int get_angle(lua_State *);
		static int get_world_center(lua_State *);
		static int get_local_center(lua_State *);

		static int set_linear_velocity(lua_State *);
		static int get_linear_velocity(lua_State *);
		static int set_angular_velocity(lua_State *);
		static int get_angular_velocity(lua_State *);

		static int apply_force(lua_State *);
		static int apply_force_to_center(lua_State *);
		static int apply_torque(lua_State *);
		static int apply_linear_impulse(lua_State *);
		static int apply_angular_impulse(lua_State *);

		static int get_linear_damping(lua_State *);
		static int set_linear_damping(lua_State *);
		static int get_angular_damping(lua_State *);
		static int set_angular_damping(lua_State *);

		static int get_gravity_scale(lua_State *);
		static int set_gravity_scale(lua_State *);

		static int set_bullet(lua_State *);
		static int is_bullet(lua_State *);

		static int set_sleeping_allowed(lua_State *);
		static int is_sleeping_allowed(lua_State *);
		static int set_awake(lua_State *);
		static int is_awake(lua_State *);
		static int set_active(lua_State *);
		static int is_active(lua_State *);

		static int set_fixed_rotation(lua_State *);
		static int is_fixed_rotation(lua_State *);

		static int dump(lua_State *);

		b2Body *body;
	};

	Body::Body(lua_State *L)
	    : body(static_cast<b2Body*>(lua_touserdata(L, -1))) { }

	b2Body* Body::get()
	{
		return this->body;
	}

	int Body::bind(lua_State *L)
	{
		LUA_ASSERT(luaR_isuserdata(L, 2), "<b2.Body>:Bind(<rainbow.sprite>)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		BodyData *data = static_cast<BodyData*>(self->body->GetUserData());
		Rainbow::Lua::replacetable(L, 2);
		data->sprite =
		    Rainbow::Lua::touserdata<Rainbow::Lua::Sprite>(L, 2)->get();
		b2Vec2 pos = self->body->GetPosition();
		pos *= ptm_ratio;
		data->sprite->set_position(Vec2f(pos.x, pos.y));
		data->sprite->set_rotation(self->body->GetAngle());
		return 0;
	}

	int Body::scale_and_position(lua_State *L)
	{
		LUA_ASSERT(lua_isnumber(L, 2) &&
		           lua_isnumber(L, 3) &&
		           lua_isnumber(L, 4),
		           "<b2.Body>:ScaleAndPosition(scale, x, y)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		const lua_Number scale = lua_tonumber(L, 2);
		const lua_Number x = lua_tonumber(L, 3);
		const lua_Number y = lua_tonumber(L, 4);

		b2BodyDef body_def;
		body_def.type = self->body->GetType();
		body_def.position.Set(x / ptm_ratio, y / ptm_ratio);
		body_def.angle = self->body->GetAngle();
		body_def.linearVelocity = self->body->GetLinearVelocity();
		body_def.angularVelocity = self->body->GetAngularVelocity();
		body_def.linearDamping = self->body->GetLinearDamping();
		body_def.angularDamping = self->body->GetAngularDamping();
		body_def.allowSleep = self->body->IsSleepingAllowed();
		body_def.awake = self->body->IsAwake();
		body_def.fixedRotation = self->body->IsFixedRotation();
		body_def.bullet = self->body->IsBullet();
		body_def.active = self->body->IsActive();
		body_def.userData = self->body->GetUserData();
		body_def.gravityScale = self->body->GetGravityScale();

		b2World *world = self->body->GetWorld();
		b2Body *new_body = world->CreateBody(&body_def);

		b2FixtureDef fixture;
		b2CircleShape shape;
		fixture.shape = &shape;
		for (b2Fixture *f = self->body->GetFixtureList(); f; f = f->GetNext())
		{
			if (f->GetType() != b2Shape::e_circle)
			{
				LUA_ASSERT(f->GetType() == b2Shape::e_circle,
				           "Unsupported shape");
				world->DestroyBody(new_body);
				return 0;
			}
			shape = *static_cast<b2CircleShape*>(f->GetShape());
			shape.m_radius *= scale;
			fixture.userData = f->GetUserData();
			fixture.friction = f->GetFriction();
			fixture.restitution = f->GetRestitution();
			fixture.density = f->GetDensity();
			fixture.isSensor = f->IsSensor();
			fixture.filter = f->GetFilterData();
			new_body->CreateFixture(&fixture);
		}

		// Reset user data
		if (body_def.userData)
		{
			BodyData *d = static_cast<BodyData*>(body_def.userData);
			d->prev_p = body_def.position;
			d->curr_p = body_def.position;
			d->prev_r = body_def.angle;
			d->curr_r = body_def.angle;
			if (d->sprite)
			{
				d->sprite->set_position(Vec2f(x, y));
				d->sprite->set_rotation(body_def.angle);
			}
		}

		// Re-register body
		lua_rawgeti(L, LUA_REGISTRYINDEX, g_body_list);
		lua_pushlightuserdata(L, self->body);
		lua_rawget(L, -2);
		lua_pushlightuserdata(L, new_body);
		lua_insert(L, -2);
		lua_rawset(L, -3);
		lua_pushlightuserdata(L, self->body);
		lua_pushnil(L);
		lua_rawset(L, -3);
		lua_pop(L, 1);

		world->DestroyBody(self->body);
		self->body = new_body;
		return 0;
	}

	int Body::create_fixture(lua_State *L)
	{
		const char err[] =
		    "<b2.Body>:CreateFixture(<b2.FixtureDef> | [<b2.Shape>, density])";
		LUA_ASSERT(
		    lua_istable(L, 2) && (lua_isnumber(L, 3) || lua_isnone(L, 3)),
		    err);

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		b2Fixture *fixture = nullptr;
		switch (lua_gettop(L))
		{
			case 2: {
				b2FixtureDef def;
				parse_FixtureDef(L, def);
				fixture = self->body->CreateFixture(&def);
				delete def.shape;
				break;
			}
			case 3: {
				const float density = lua_tonumber(L, 3);
				lua_pop(L, 1);
				std::unique_ptr<b2Shape> shape(parse_Shape(L));
				fixture = self->body->CreateFixture(shape.get(), density);
				break;
			}
			default:
				R_ASSERT(false, err);
				break;
		}
		lua_pushlightuserdata(L, fixture);
		return 1;
	}

	int Body::destroy_fixture(lua_State *L)
	{
		LUA_ASSERT(lua_isuserdata(L, 2),
		           "<b2.Body>:DestroyFixture(<b2.Fixture>)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->DestroyFixture(
		    static_cast<b2Fixture*>(lua_touserdata(L, 2)));
		return 0;
	}

	int Body::set_transform(lua_State *L)
	{
		LUA_ASSERT(lua_isnumber(L, 2) &&
		           lua_isnumber(L, 3) &&
		           lua_isnumber(L, 4),
		           "<b2.Body>:SetTransform(x, y, r)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->SetTransform(
		    b2Vec2(lua_tonumber(L, 2) / ptm_ratio,
		           lua_tonumber(L, 3) / ptm_ratio),
		    lua_tonumber(L, 4));
		return 0;
	}

	int Body::get_position(lua_State *L)
	{
		Body *self = Bind::self(L);
		if (!self)
			return 0;

		const b2Vec2 &pos = self->body->GetPosition();
		lua_pushnumber(L, pos.x * ptm_ratio);
		lua_pushnumber(L, pos.y * ptm_ratio);
		return 2;
	}

	int Body::get_angle(lua_State *L)
	{
		Body *self = Bind::self(L);
		if (!self)
			return 0;

		lua_pushnumber(L, self->body->GetAngle());
		return 1;
	}

	int Body::get_world_center(lua_State *L)
	{
		Body *self = Bind::self(L);
		if (!self)
			return 0;

		const b2Vec2 &center = self->body->GetWorldCenter();
		lua_pushnumber(L, center.x * ptm_ratio);
		lua_pushnumber(L, center.y * ptm_ratio);
		return 2;
	}

	int Body::get_local_center(lua_State *L)
	{
		Body *self = Bind::self(L);
		if (!self)
			return 0;

		const b2Vec2 &center = self->body->GetLocalCenter();
		lua_pushnumber(L, center.x * ptm_ratio);
		lua_pushnumber(L, center.y * ptm_ratio);
		return 2;
	}

	int Body::set_linear_velocity(lua_State *L)
	{
		LUA_ASSERT(lua_isnumber(L, 2) && lua_isnumber(L, 3),
		           "<b2.Body>:SetLinearVelocity(x, y)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->SetLinearVelocity(
		    b2Vec2(lua_tonumber(L, 2), lua_tonumber(L, 3)));
		return 0;
	}

	int Body::get_linear_velocity(lua_State *L)
	{
		Body *self = Bind::self(L);
		if (!self)
			return 0;

		const b2Vec2 &v = self->body->GetLinearVelocity();
		lua_pushnumber(L, v.x);
		lua_pushnumber(L, v.y);
		return 2;
	}

	int Body::set_angular_velocity(lua_State *L)
	{
		LUA_ASSERT(lua_isnumber(L, 2), "<b2.Body>:SetAngularVelocity(v)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->SetAngularVelocity(lua_tonumber(L, 2));
		return 0;
	}

	int Body::get_angular_velocity(lua_State *L)
	{
		Body *self = Bind::self(L);
		if (!self)
			return 0;

		lua_pushnumber(L, self->body->GetAngularVelocity());
		return 1;
	}

	int Body::apply_force(lua_State *L)
	{
		LUA_ASSERT(lua_isnumber(L, 2) &&
		           lua_isnumber(L, 3) &&
		           lua_isnumber(L, 4) &&
		           lua_isnumber(L, 5) &&
		           lua_isboolean(L, 6),
		           "<b2.Body>:ApplyForce(impulse.x, impulse.y, point.x, point.y, wake)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->ApplyForce(
		    b2Vec2(lua_tonumber(L, 2), lua_tonumber(L, 3)),
		    b2Vec2(lua_tonumber(L, 4) / ptm_ratio,
		           lua_tonumber(L, 5) / ptm_ratio),
		    lua_toboolean(L, 6));
		return 0;
	}

	int Body::apply_force_to_center(lua_State *L)
	{
		LUA_ASSERT(lua_isnumber(L, 2) &&
		           lua_isnumber(L, 3) &&
		           lua_isboolean(L, 4),
		           "<b2.Body>:ApplyForceToCenter(impulse.x, impulse.y, wake)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->ApplyForceToCenter(
		    b2Vec2(lua_tonumber(L, 2), lua_tonumber(L, 3)),
		    lua_toboolean(L, 4));
		return 0;
	}

	int Body::apply_torque(lua_State *L)
	{
		LUA_ASSERT(lua_isnumber(L, 2) && lua_isboolean(L, 3),
		           "<b2.Body>:ApplyTorque(torque, wake)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->ApplyTorque(lua_tonumber(L, 2), lua_toboolean(L, 3));
		return 0;
	}

	int Body::apply_linear_impulse(lua_State *L)
	{
		LUA_ASSERT(lua_isnumber(L, 2) &&
		           lua_isnumber(L, 3) &&
		           lua_isnumber(L, 4) &&
		           lua_isnumber(L, 5) &&
		           lua_isboolean(L, 6),
		           "<b2.Body>:ApplyLinearImpulse(impulse.x, impulse.y, point.x, point.y, wake)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->ApplyLinearImpulse(
		    b2Vec2(lua_tonumber(L, 2), lua_tonumber(L, 3)),
		    b2Vec2(lua_tonumber(L, 4) / ptm_ratio,
		           lua_tonumber(L, 5) / ptm_ratio),
		    lua_toboolean(L, 6));
		return 0;
	}

	int Body::apply_angular_impulse(lua_State *L)
	{
		LUA_ASSERT(lua_isnumber(L, 2) && lua_isboolean(L, 3),
		           "<b2.Body>:ApplyAngularImpulse(impulse, wake)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->ApplyAngularImpulse(lua_tonumber(L, 2),
		                                lua_toboolean(L, 3));
		return 0;
	}

	int Body::get_linear_damping(lua_State *L)
	{
		Body *self = Bind::self(L);
		if (!self)
			return 0;

		lua_pushnumber(L, self->body->GetLinearDamping() * ptm_ratio);
		return 1;
	}

	int Body::set_linear_damping(lua_State *L)
	{
		LUA_ASSERT(lua_isnumber(L, 2), "<b2.Body>:SetLinearDamping(damping)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->SetLinearDamping(lua_tonumber(L, 2) / ptm_ratio);
		return 0;
	}

	int Body::get_angular_damping(lua_State *L)
	{
		Body *self = Bind::self(L);
		if (!self)
			return 0;

		lua_pushnumber(L, self->body->GetAngularDamping());
		return 1;
	}

	int Body::set_angular_damping(lua_State *L)
	{
		LUA_ASSERT(lua_isnumber(L, 2), "<b2.Body>:SetAngularDamping(damping)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->SetAngularDamping(lua_tonumber(L, 2));
		return 0;
	}

	int Body::get_gravity_scale(lua_State *L)
	{
		Body *self = Bind::self(L);
		if (!self)
			return 0;

		lua_pushnumber(L, self->body->GetGravityScale());
		return 1;
	}

	int Body::set_gravity_scale(lua_State *L)
	{
		LUA_ASSERT(lua_isnumber(L, 2), "<b2.Body>:SetGravityScale(scale)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->SetGravityScale(lua_tonumber(L, 2));
		return 0;
	}

	int Body::set_bullet(lua_State *L)
	{
		LUA_ASSERT(lua_isboolean(L, 2), "<b2.Body>:SetBullet(bool)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->SetBullet(lua_toboolean(L, 2));
		return 0;
	}

	int Body::is_bullet(lua_State *L)
	{
		Body *self = Bind::self(L);
		if (!self)
			return 0;

		lua_pushboolean(L, self->body->IsBullet());
		return 1;
	}

	int Body::set_sleeping_allowed(lua_State *L)
	{
		LUA_ASSERT(lua_isboolean(L, 2), "<b2.Body>:SetSleepingAllowed(bool)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->SetSleepingAllowed(lua_toboolean(L, 2));
		return 0;
	}

	int Body::is_sleeping_allowed(lua_State *L)
	{
		Body *self = Bind::self(L);
		if (!self)
			return 0;

		lua_pushboolean(L, self->body->IsSleepingAllowed());
		return 1;
	}

	int Body::set_awake(lua_State *L)
	{
		LUA_ASSERT(lua_isboolean(L, 2), "<b2.Body>:SetAwake(bool)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->SetAwake(lua_toboolean(L, 2));
		return 0;
	}

	int Body::is_awake(lua_State *L)
	{
		Body *self = Bind::self(L);
		if (!self)
			return 0;

		lua_pushboolean(L, self->body->IsAwake());
		return 1;
	}

	int Body::set_active(lua_State *L)
	{
		LUA_ASSERT(lua_isboolean(L, 2), "<b2.Body>:SetActive(bool)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->SetActive(lua_toboolean(L, 2));
		return 0;
	}

	int Body::is_active(lua_State *L)
	{
		Body *self = Bind::self(L);
		if (!self)
			return 0;

		lua_pushboolean(L, self->body->IsActive());
		return 0;
	}

	int Body::set_fixed_rotation(lua_State *L)
	{
		LUA_ASSERT(lua_isboolean(L, 2), "<b2.Body>:SetFixedRotation(bool)");

		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->SetFixedRotation(lua_toboolean(L, 2));
		return 0;
	}

	int Body::is_fixed_rotation(lua_State *L)
	{
		Body *self = Bind::self(L);
		if (!self)
			return 0;

		lua_pushboolean(L, self->body->IsFixedRotation());
		return 0;
	}

	int Body::dump(lua_State *L)
	{
		Body *self = Bind::self(L);
		if (!self)
			return 0;

		self->body->Dump();
		return 0;
	}
} NS_B2_LUA_END

NS_RAINBOW_LUA_BEGIN
{
	using b2::Lua::Body;

	template<>
	const char Body::Bind::class_name[] = "Body";

	template<>
	const bool Body::Bind::is_constructible = true;

	template<>
	const luaL_Reg Body::Bind::functions[] = {
		{ "Bind",                 &Body::bind },
		{ "ScaleAndPosition",     &Body::scale_and_position },
		{ "CreateFixture",        &Body::create_fixture },
		{ "DestroyFixture",       &Body::destroy_fixture },
		{ "SetTransform",         &Body::set_transform },
		{ "GetPosition",          &Body::get_position },
		{ "GetAngle",             &Body::get_angle },
		{ "GetWorldCenter",       &Body::get_world_center },
		{ "GetLocalCenter",       &Body::get_local_center },
		{ "SetLinearVelocity",    &Body::set_linear_velocity },
		{ "GetLinearVelocity",    &Body::get_linear_velocity },
		{ "SetAngularVelocity",   &Body::set_angular_velocity },
		{ "GetAngularVelocity",   &Body::get_angular_velocity },
		{ "ApplyForce",           &Body::apply_force },
		{ "ApplyForceToCenter",   &Body::apply_force_to_center },
		{ "ApplyTorque",          &Body::apply_torque },
		{ "ApplyLinearImpulse",   &Body::apply_linear_impulse },
		{ "ApplyAngularImpulse",  &Body::apply_angular_impulse },
		{ "GetLinearDamping",     &Body::get_linear_damping },
		{ "SetLinearDamping",     &Body::set_linear_damping },
		{ "GetAngularDamping",    &Body::get_angular_damping },
		{ "SetAngularDamping",    &Body::set_angular_damping },
		{ "GetGravityScale",      &Body::get_gravity_scale },
		{ "SetGravityScale",      &Body::set_gravity_scale },
		{ "SetBullet",            &Body::set_bullet },
		{ "IsBullet",             &Body::is_bullet },
		{ "SetSleepingAllowed",   &Body::set_sleeping_allowed },
		{ "IsSleepingAllowed",    &Body::is_sleeping_allowed },
		{ "SetAwake",             &Body::set_awake },
		{ "IsAwake",              &Body::is_awake },
		{ "SetActive",            &Body::set_active },
		{ "IsActive",             &Body::is_active },
		{ "SetFixedRotation",     &Body::set_fixed_rotation },
		{ "IsFixedRotation",      &Body::is_fixed_rotation },
		{ "Dump",                 &Body::dump },
		{ nullptr,                nullptr }
	};
} NS_RAINBOW_LUA_END
