// Copyright (c) 2010-14 Bifrost Entertainment AS and Tommy Nguyen
// Distributed under the MIT License.
// (See accompanying file LICENSE or copy at http://opensource.org/licenses/MIT)

#ifndef SERVICES_TESTFLIGHT_H_
#define SERVICES_TESTFLIGHT_H_

struct lua_State;

namespace Rainbow
{
	namespace Services
	{
		namespace TestFlight
		{
			void init(lua_State *);
		}
	}
}

#endif
