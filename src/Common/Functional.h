// Copyright (c) 2010-14 Bifrost Entertainment AS and Tommy Nguyen
// Distributed under the MIT License.
// (See accompanying file LICENSE or copy at http://opensource.org/licenses/MIT)

#ifndef COMMON_FUNCTIONAL_H_
#define COMMON_FUNCTIONAL_H_

#ifndef _MSC_VER
#	define pure __attribute__((const))
#else
#	define pure
#endif

#endif
