<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Rainbow API Reference</title>
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/monokai_sublime.css">
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body>
    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="javascript:window.scroll(0,0);">Rainbow API Reference</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="internal-api">
                Internal API <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" aria-labelledby="internal-api">
                <li><a href="#rainbow-animation">animation</a></li>
                <li><a href="#rainbow-audio">audio</a></li>
                <li><a href="#rainbow-font">font</a></li>
                <li><a href="#rainbow-input">input</a></li>
                <li><a href="#rainbow-io">io</a></li>
                <li><a href="#rainbow-label">label</a></li>
                <li><a href="#rainbow-platform">platform</a></li>
                <li><a href="#rainbow-random">random</a></li>
                <li><a href="#rainbow-renderer">renderer</a></li>
                <li><a href="#rainbow-scenegraph">scenegraph</a></li>
                <li><a href="#rainbow-sprite">sprite</a></li>
                <li><a href="#rainbow-spritebatch">spritebatch</a></li>
                <li><a href="#rainbow-texture">texture</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="libraries">
                Libraries <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" aria-labelledby="libraries">
                <li><a href="#coroutine">Coroutine</a></li>
                <li><a href="#functional">Functional</a></li>
                <li><a href="#math">Math</a></li>
                <li><a href="#parallax">Parallax</a></li>
                <li><a href="#stack">Stack</a></li>
                <li><a href="#timer">Timer</a></li>
                <li><a href="#transition">Transition</a></li>
                <li class="divider"></li>
                <li><a href="http://box2d.org/documentation/">Box2D</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.navbar-collapse -->
      </div>
    </div>
    <div id="top"><a href="javascript:window.scroll(0,0);">Top &uarr;</a></div>
    <div id="content" class="container">

  * [animation](#rainbow-animation)
  * [audio](#rainbow-audio)
  * [font](#rainbow-font)
  * [input](#rainbow-input)
  * [io](#rainbow-io)
  * [label](#rainbow-label)
  * [platform](#rainbow-platform)
  * [random](#rainbow-random)
  * [renderer](#rainbow-renderer)
  * [scenegraph](#rainbow-scenegraph)
  * [sprite](#rainbow-sprite)
  * [spritebatch](#rainbow-spritebatch)
  * [texture](#rainbow-texture)
  * &mdash;
  * [Coroutine](#coroutine)
  * [Functional](#functional)
  * [Math](#math)
  * [Parallax](#parallax)
  * [Stack](#stack)
  * [Timer](#timer)
  * [Transition](#transition)
  * &mdash;
  * [Box2D](http://box2d.org/documentation/)

## rainbow.animation

Sprite animations use separate textures within a [texture atlas](#rainbow-texture) as animation frames. Since animations are bound to a [sprite](#rainbow-sprite), which in turn is bound to a [sprite batch](#rainbow-spritebatch), they are also bound to the [texture atlas](#rainbow-texture) the batch is using.

### rainbow.animation(sprite, frames, fps, delay)

| Parameter | Description |
|-----------|-------------|
| <var>sprite</var> | The sprite object to animate. |
| <var>frames</var> | Array of frames that make up the animation. |
| <var>fps</var> | Number of frames per second to animate at. |
| <var>delay</var> | Number of frames to delay before looping. Negative numbers disable looping. Default: 0. |

Creates a sprite animation.

### &lt;rainbow.animation&gt;:is_stopped()

Returns whether the animation has stopped.

### &lt;rainbow.animation&gt;:set_delay(delay)

| Parameter | Description |
|-----------|-------------|
| <var>delay</var> | Number of milliseconds to delay before looping. |

Sets the number of milliseconds to delay before looping.

### &lt;rainbow.animation&gt;:set_fps(fps)

| Parameter | Description |
|-----------|-------------|
| <var>fps</var> | Number of frames per second to animate at. |

Sets the number of frames per second to animate at.

### &lt;rainbow.animation&gt;:set_frames(frames)

| Parameter | Description |
|-----------|-------------|
| <var>frames</var> | Table of texture indices that make up the animation. |

Sets table of animation frames.

### &lt;rainbow.animation&gt;:set_sprite(sprite)

| Parameter | Description |
|-----------|-------------|
| <var>sprite</var> | The sprite to animate. |

Sets the sprite to animate.

### &lt;rainbow.animation&gt;:play()

Starts the animation.

### &lt;rainbow.animation&gt;:stop()

Stops the animation.

## rainbow.audio

Audio consists mainly of the sound object and the audio channel. The sound object is basically an audio buffer. It can be wholly loaded, or it can stream from disk. A sound object is played on an audio channel. An audio channel can only play one sound object at a time but the sound object can be used by any number of channels. As raw audio data can take a large amount of memory, it is recommended to only create static sound objects for short audio files (such as sound effects).

Officially, Rainbow supports only [Ogg Vorbis](http://en.wikipedia.org/wiki/Vorbis) audio format. However, on iOS, the [list of supported audio formats](http://developer.apple.com/library/ios/#documentation/AudioVideo/Conceptual/MultimediaPG/UsingAudio/UsingAudio.html#//apple_ref/doc/uid/TP40009767-CH2-SW9) includes AAC (MPEG-4 Advanced Audio Coding), ALAC (Apple Lossless), HE-AAC (MPEG-4 High Efficiency AAC), iLBC (internet Low Bit Rate Codec), IMA4 (IMA/ADPCM), Linear PCM (uncompressed, linear pulse code modulation), MP3, µ-law and a-law. Of these, AAC, ALAC, HE-AAC and MP3 are hardware-assisted. Mac OS X also supports these in addition to [Ogg Vorbis](http://en.wikipedia.org/wiki/Vorbis). On Android, the list of supported audio formats vary with each device but MP3 and Ogg Vorbis are both safe bets.

### rainbow.audio.clear()

Stops and deletes all sound objects.

### rainbow.audio.create_sound(path, +mode, +loops)

| Parameter | Description |
|-----------|-------------|
| <var>path</var> | Path to audio source, relative to the location of the main script. |
| <var>mode</var> | <span class="optional">Load into buffer (0), or stream from disk (1). Default: 0.</span> |
| <var>loops</var> | <span class="optional">Number of times to loop. Only applicable if streaming. Default: -1.</span> |

Creates a sound object. This object is just a reference to the audio source and cannot be manipulated in any way.

### rainbow.audio.delete_sound(sound)

| Parameter | Description |
|-----------|-------------|
| <var>sound</var> | The sound object to delete. |

Deletes sound object. This will stop all channels from using the object and release all related resources.

### rainbow.audio.pause(channel)

| Parameter | Description |
|-----------|-------------|
| <var>channel</var> | The channel to pause playback. |

Sets channel on pause.

### rainbow.audio.play(sound)

| Parameter | Description |
|-----------|-------------|
| <var>sound</var> | The sound object to play. |

Starts playback of the sound object and returns the channel on which it is played. Channels may vary with each playback.

### rainbow.audio.set_gain(+channel, volume)

| Parameter | Description |
|-----------|-------------|
| <var>channel</var> | <span class="optional">The channel to change gain/volume. Default: master channel.</span> |
| <var>volume</var> | Desired gain/volume. Valid values: 0.0-1.0. |

Sets channel gain/volume.

### rainbow.audio.set_pitch(pitch)

| Parameter | Description |
|-----------|-------------|
| <var>pitch</var> | Desired pitch shift, where 1.0 equals identify. Each reduction by 50 percent equals a pitch shift of -12 semitones (one octave reduction). Each doubling equals a pitch shift of 12 semitones (one octave increase). Zero is not a legal value. |

Sets global pitch shift.

### rainbow.audio.stop(channel)

| Parameter | Description |
|-----------|-------------|
| <var>channel</var> | The channel to stop playback. |

Stops channel.

## rainbow.font

Font objects are used by [labels](#rainbow-label) to display text. Like textures, it is recommended to reuse them whenever possible. A font object is created with a fixed point size and cannot be resized. If a different size is desired, a new font object must be created.

Rainbow currently supports OpenType and TrueType fonts.

### rainbow.font(path, size)

| Parameter | Description |
|-----------|-------------|
| <var>path</var> | Path to font, relative to the location of the main script. |
| <var>size</var> | Point size. |

Creates a font with a fixed point size.

## rainbow.input

Input events are only sent to objects that subscribe to them. Such objects are called event listeners. A listener can be implemented as follows.

    InputListener = {}
    InputListener.__index = InputListener

    function InputListener:key_down(key, modifiers) end
    function InputListener:key_up(key, modifiers) end
    function InputListener:touch_ended(touches) end
    function InputListener:touch_canceled() end
    function InputListener:touch_moved(touches) end

    # Create our listener and let it subscribe to input events.
    local mylistener = setmetatable({}, InputListener)
    rainbow.input.subscribe(mylistener)

    # We're only interested in touch began events.
    function mylistener:touch_began(touches)
      for hash,touch in pairs(touches) do
        # Handle event here
      end
    end

As seen in the example, the easiest way is to define an ``InputListener`` and inherit from it, then define the functions that are needed. The important point here is that all event handlers must be implemented even if they'll do nothing.

For touch events, a table of events are sent with each notification. It is iterated as above. The ``hash`` value uniquely identifies a touch (or mouse button) for the duration of it touching the screen (or mouse button being held). Touch (or mouse click) location is stored in ``touch``:

    touch.x          # For the x-coordinate.
    touch.y          # For the y-coordinate.
    touch.timestamp  # For the relative time at which the event occurred.

_Desktop-only:_ Keyboard event listeners receive the key value (the actual key that was pressed/released) of the event and, if available, its modifiers (i.e. ctrl, alt or shift).

### rainbow.input.subscribe(listener)

| Parameter | Description |
|-----------|-------------|
| <var>listener</var> | Input listener object. |

Adds an input listener.

### rainbow.input.unsubscribe(listener)

| Parameter | Description |
|-----------|-------------|
| <var>listener</var> | Input listener object. |

Removes an input listener.

### rainbow.input.unsubscribe_all()

Removes all input listeners.

## rainbow.io

### rainbow.io.load()

### rainbow.io.save()

## rainbow.label

A label is used to display text.

### rainbow.label(+text)

| Parameter | Description |
|-----------|-------------|
| <var>text</var> | <span class="optional">The text to be displayed on the label. Default: "".</span> |

### &lt;rainbow.label&gt;:get_color()

Returns the font colour in separate channels (RGBA).

### &lt;rainbow.label&gt;:set_alignment(align)

| Parameter | Description |
|-----------|-------------|
| <var>align</var> | Text alignment. Valid values: 'l', 'c', 'r'. Default: 'l'. |

Sets text alignment.

### &lt;rainbow.label&gt;:set_color(r, g, b, +a)

| Parameter | Description |
|-----------|-------------|
| <var>r</var> | Amount of red. Valid values: 0-255. |
| <var>g</var> | Amount of green. Valid values: 0-255. |
| <var>b</var> | Amount of blue. Valid values: 0-255. |
| <var>a</var> | <span class="optional">Amount of opacity. Valid values: 0-255. Default: 255.</span> |

Sets text colour.

### &lt;rainbow.label&gt;:set_font(font)

| Parameter | Description |
|-----------|-------------|
| <var>r</var> | [Font type](#rainbow-font) to use. |

Sets font type.

### &lt;rainbow.label&gt;:set_position(x, y)

| Parameter | Description |
|-----------|-------------|
| <var>x, y</var> | Position. |

Sets label position.

### &lt;rainbow.label&gt;:set_scale(scale)

| Parameter | Description |
|-----------|-------------|
| <var>scale</var> | Factor to scale label by. Valid values: 0.01-1.0. |

Sets label scale. Values are clamped between 0.01-1.0.

### &lt;rainbow.label&gt;:set_text(text)

| Parameter | Description |
|-----------|-------------|
| <var>text</var> | Text to be displayed on the label. |

Sets the text to be displayed on the label.

## rainbow.platform

Query the system for capabilities or sensor readings.

### rainbow.platform.accelerometer

| Parameter | Description |
|-----------|-------------|
| <var>timestamp</var> | The relative time at which the acceleration event occurred. |
| <var>x</var> | The acceleration value for the x axis of the device. With the device held in portrait orientation and the screen facing you, the x axis runs from left (negative values) to right (positive values) across the face of the device. |
| <var>y</var> | The acceleration value for the y axis of the device. With the device held in portrait orientation and the screen facing you, the y axis runs from bottom (negative values) to top (positive values) across the face of the device. |
| <var>z</var> | The acceleration value for the z axis of the device. With the device held in portrait orientation and the screen facing you, the z axis runs from back (negative values) to front (positive values) through the device. |

### rainbow.platform.screen

| Parameter | Description |
|-----------|-------------|
| <var>width</var> | Screen width in pixels. |
| <var>height</var> | Screen height in pixels. |
| <var>touch</var> | Whether the screen has touch capabilities. |

### rainbow.platform.memory

Total amount of RAM in MB.

## rainbow.random

Rainbow's pseudo-random number generator is the [Double precision SIMD-oriented Fast Mersenne Twister](http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/SFMT/) developed by Mutsuo Saito and Makoto Matsumoto at Hiroshima University and University of Tokyo. The implementation in use supports a period of 2<sup>19937</sup> - 1.

``rainbow.seed()`` must be called before using this module.

### rainbow.random()

Returns a random number in interval [0, 1).

### rainbow.random(max)

| Parameter | Description |
|-----------|-------------|
| <var>max</var> | Maximum value. |

Returns a random number in interval [0, <var>max</var>).

### rainbow.random(min, max)

| Parameter | Description |
|-----------|-------------|
| <var>min</var> | Minimum value. |
| <var>max</var> | Maximum value. |

Returns a random number in interval [<var>min</var>, <var>max</var>).

### rainbow.seed(+seed)

| Parameter | Description |
|-----------|-------------|
| <var>seed</var> | <span class="optional">Random number generator seed. Default: 0 (current system time).</span> |

Sets the random number generator seed. Must be called before any other calls.

## rainbow.renderer

Access low-level graphics methods and properties.

### rainbow.renderer.max_texture_size

Rough estimate of the largest texture that the platform can handle.

### rainbow.renderer.supports_pvrtc

Whether the platform supports PVRTC textures.

### rainbow.renderer.set_clear_color(r, g, b)

| Parameter | Description |
|-----------|-------------|
| <var>r</var> | Amount of red. Valid values: 0-255. |
| <var>g</var> | Amount of green. Valid values: 0-255. |
| <var>b</var> | Amount of blue. Valid values: 0-255. |

Sets clear colour. Clearing occurs at the start of each drawn frame.

### rainbow.renderer.set_filter(filter)

| Parameter | Description |
|-----------|-------------|
| <var>filter</var> | Texture filtering method. Valid values: ``gl.NEAREST``, ``gl.LINEAR``. |

Sets texture filtering method. Only affects new textures.

### rainbow.renderer.set_projection(left, right, bottom, top)

| Parameter | Description |
|-----------|-------------|
| <var>left</var> | Leftmost point in pixels. Default: 0. |
| <var>right</var> | Rightmost point in pixels. Default: [``rainbow.platform.screen.width``](#rainbow-platform-screen). |
| <var>bottom</var> | Bottommost point in pixels. Default: 0. |
| <var>top</var> | Topmost point in pixels. Default: [``rainbow.platform.screen.height``](#rainbow-platform-screen). |

Sets orthographic projection.

## rainbow.scenegraph

Drawables must be attached to the scene graph in order to be updated and drawn. The scene graph is traversed in a depth-first manner. In a single node, this means that its children are updated and drawn in the order they were created.

### &lt;rainbow.scenegraph&gt;:add_animation(+parent, animation)

| Parameter | Description |
|-----------|-------------|
| <var>parent</var> | <span class="optional">Parent node to attach to. Default: root.</span> |
| <var>animation</var> | The [animation](#rainbow-animation) to attach to the graph. |

Creates a node containing a [sprite animation](#rainbow-animation).

### &lt;rainbow.scenegraph&gt;:add_batch(+parent, spritebatch)

| Parameter | Description |
|-----------|-------------|
| <var>parent</var> | <span class="optional">Parent node to attach to. Default: root.</span> |
| <var>spritebatch</var> | The [sprite batch](#rainbow-spritebatch) to attach to the graph. |

Creates a node containing a [sprite batch](#rainbow-spritebatch).

### &lt;rainbow.scenegraph&gt;:add_drawable(+parent, drawable)

| Parameter | Description |
|-----------|-------------|
| <var>parent</var> | <span class="optional">Parent node to attach to. Default: root.</span> |
| <var>drawable</var> | The drawable object to attach to the graph. |

Creates a node containing a drawable.

### &lt;rainbow.scenegraph&gt;:add_node(+parent)

| Parameter | Description |
|-----------|-------------|
| <var>parent</var> | <span class="optional">Parent node to attach to. Default: root.</span> |

Creates a group node.

### &lt;rainbow.scenegraph&gt;:add_label(+parent, label)

| Parameter | Description |
|-----------|-------------|
| <var>parent</var> | <span class="optional">Parent node to attach to. Default: root.</span> |
| <var>label</var> | The [label](#rainbow-label) to attach to the graph. |

Creates a node containing a [label](#rainbow-label).

### &lt;rainbow.scenegraph&gt;:disable(node)

| Parameter | Description |
|-----------|-------------|
| <var>node</var> | The node to disable. |

Disables a node. Disabling a node will cut short the graph traversal, effectively disabling all children nodes but still maintain their states.

### &lt;rainbow.scenegraph&gt;:enable(node)

| Parameter | Description |
|-----------|-------------|
| <var>node</var> | The node to enable. |

Enables a node. Enabling a node will open up the path in the graph, effectively enabling all children nodes but still maintain their states.

### &lt;rainbow.scenegraph&gt;:move(node, x, y)

| Parameter | Description |
|-----------|-------------|
| <var>node</var> | The node to move. |
| <var>x, y</var> | Amount to move the node by. |

Moves a node and all of its children by (x,y).

### &lt;rainbow.scenegraph&gt;:remove(node)

| Parameter | Description |
|-----------|-------------|
| <var>node</var> | The node to remove. |

Removes a node and all of its children from the graph.

### &lt;rainbow.scenegraph&gt;:set_parent(parent, node)

| Parameter | Description |
|-----------|-------------|
| <var>parent</var> | The new parent. |
| <var>node</var> | The node to move. |

Moves node to a new parent node.

## rainbow.sprite

A sprite is a textured quad in a coordinate system with the origin at the lower left corner of the screen. Sprites are created by a [sprite batch](#rainbow-spritebatch) and uses the [texture atlas](#rainbow-texture) assigned to the batch.

### &lt;rainbow.sprite&gt;:get_angle()

Returns sprite orientation in radians.

### &lt;rainbow.sprite&gt;:get_color()

Returns sprite colour in separate channels (RGBA).

### &lt;rainbow.sprite&gt;:get_position()

Returns sprite position.

### &lt;rainbow.sprite&gt;:set_color(r, g, b, +a)

| Parameter | Description |
|-----------|-------------|
| <var>r</var> | Amount of red. Valid values: 0-255. |
| <var>g</var> | Amount of green. Valid values: 0-255. |
| <var>b</var> | Amount of blue. Valid values: 0-255. |
| <var>a</var> | <span class="optional">Amount of opacity. Valid values: 0-255. Default: 255.</span> |

Sets sprite colour.

### &lt;rainbow.sprite&gt;:set_pivot(x, y)

| Parameter | Description |
|-----------|-------------|
| <var>x, y</var> | Pivot point. Default: 0.5, 0.5. |

Sets sprite's pivot point.

### &lt;rainbow.sprite&gt;:set_position(x, y)

| Parameter | Description |
|-----------|-------------|
| <var>x, y</var> | Position. Default: 0, 0. |

Sets sprite position.

### &lt;rainbow.sprite&gt;:set_rotation(r)

| Parameter | Description |
|-----------|-------------|
| <var>r</var> | Angle in radians. Default: 0.0. |

Sets sprite rotation.

### &lt;rainbow.sprite&gt;:set_scale(f)

| Parameter | Description |
|-----------|-------------|
| <var>f</var> | Scale factor. Default: 1.0. |

Sets sprite scale.

### &lt;rainbow.sprite&gt;:set_texture(texture)

| Parameter | Description |
|-----------|-------------|
| <var>texture</var> | Texture id. |

Assigns texture to sprite.

### &lt;rainbow.sprite&gt;:mirror()

Horizontally mirrors sprite's current texture.

### &lt;rainbow.sprite&gt;:move(x, y)

| Parameter | Description |
|-----------|-------------|
| <var>x, y</var> | Amount to move sprite by. |

Moves sprite by (x,y).

### &lt;rainbow.sprite&gt;:rotate(r)

| Parameter | Description |
|-----------|-------------|
| <var>r</var> | Amount to rotate sprite by. |

Rotates sprite by given angle.

## rainbow.spritebatch

Sprite batches are meant to enforce grouping of [sprites](#rainbow-sprite) in order to avoid drawing each [sprite](#rainbow-sprite) separately.

The sprites in a batch are drawn using [painter's algorithm](http://en.wikipedia.org/wiki/Painter's_algorithm) and should therefore be created in the order they want to be drawn.

### rainbow.spritebatch(+hint)

| Parameter | Description |
|-----------|-------------|
| <var>hint</var> | <span class="optional">Number of [sprites](#rainbow-sprite) to make space for. Default: 4.</span> |

Creates a batch of [sprites](#rainbow-sprite).

### &lt;rainbow.spritebatch&gt;:create_sprite(width, height)

| Parameter | Description |
|-----------|-------------|
| <var>width, height</var> | Dimension of the [sprite](#rainbow-sprite) to create. |

Creates an untextured [sprite](#rainbow-sprite) with given dimension and places it at origin.

### &lt;rainbow.spritebatch&gt;:set_texture(texture)

| Parameter | Description |
|-----------|-------------|
| <var>texture</var> | [Texture atlas](#rainbow-texture) used by all [sprites](#rainbow-sprite) in the batch. |

Sets [texture atlas](#rainbow-texture).

## rainbow.texture

Texture objects are images decoded and sent to the graphics card as texture. Textures are normally stored as raw bitmaps unless they were stored in a compressed format supported by the platform (e.g. ETC1 or PVRTC). This means that a 1024x1024 texture will normally occupy 4MB. In order to save memory, they are assumed to be [atlases](http://en.wikipedia.org/wiki/Texture_atlas) and should be reused whenever possible.

Rainbow currently supports PNG and PVRTC.

_Note:_ Textures should be square and its sides a power of two (greater than or equal to 64). This is due to how the graphics pipeline works. Even if textures do not meet this recommendation, the graphics drivers will enlarge a texture in order to do so anyway, wasting memory. The maximum size of a texture can be queried in [``rainbow.renderer``](#rainbow-renderer-max_texture_size).

### rainbow.texture(path)

| Parameter | Description |
|-----------|-------------|
| <var>path</var> | Path to texture to load. |

Creates a texture object, usable in [sprite batches](#rainbow-spritebatch).

### &lt;rainbow.texture&gt;:create(x, y, width, height)

| Parameter | Description |
|-----------|-------------|
| <var>x, y</var> | Upper left point of texture. |
| <var>width, height</var> | Dimension of texture. |

Creates/defines a texture in the atlas. The unique identifier returned can be used in [sprites](#rainbow-sprite).

## Coroutine

    local Coroutine = require(module_path .. "Coroutine")

Wrapper around Lua [Coroutines](http://lua-users.org/wiki/CoroutinesTutorial)

### Coroutine.start(function)

| Parameter | Description |
|-----------|-------------|
| <var>function</var> | Function to execute in a coroutine. |

Creates and starts a coroutine.

### Coroutine.wait(milliseconds)

| Parameter | Description |
|-----------|-------------|
| <var>milliseconds</var> | Number of milliseconds to wait before continuing. |

Blocks a coroutine for a certain amount of time.

## Functional

    local Functional = require(module_path .. "Functional")

Functions from functional programming languages such as Haskell and Scheme.

### Functional.compose(f, g)

Returns <var>f ∘ g</var>, i.e. the composition of two functions so that the result of function <var>g</var> becomes the input to function <var>f</var>. Example:

    local printf = Functional.compose(io.write, string.format)
    printf("a = %i, b = %i, c = %i\n", 1, 2, 3)
    > a = 1, b = 2, c = 3

### Functional.filter(pred, list)

Returns a copy of the list containing only the elements that satisfy the predicate.

### Functional.filterh(pred, table)

Returns a copy of the hash table containing only the elements that satisfy the predicate.

### Functional.foldl(op, z, list)

Reduces the list using the binary operator, from left to right, with <var>z</var> as starting value.

### Functional.foldl1(op, list)

A variant of <code>foldl</code> that uses the leftmost element as starting value.

### Functional.foldr(op, z, list)

Reduces the list using the binary operator, from right to left, with <var>z</var> as starting value.

### Functional.foldr1(op, list)

A variant of <code>foldr</code> that uses the rightmost element as starting value.

### Functional.map(f, list)

Returns a copy of the list with the function <var>f</var> applied to each element.

### Functional.maph(f, table)

Returns a copy of the hash table with the function <var>f</var> applied to each element.

### Functional.difference(list)

Computes the difference of a finite list of numbers; the first value being the minuend.

### Functional.product(list)

Computes the product of a finite list of numbers.

### Functional.quotient(list)

Computes the quotient of a finite list of numbers; the first value being the numerator.

### Functional.sum(list)

Computes the sum of a finite list of numbers.

## Math

    local Math = require(module_path .. "Math")

Useful mathematical functions.

### Math.G

The universal gravitation constant, defined as G = 6.67384 * 10<sup>-11</sup> N(m/kg)<sup>2</sup>.

### Math.g

Standard gravitational acceleration, defined in m/s<sup>2</sup>.

### Math.angle(a.x, a.y, b.x, b.y)

| Parameter | Description |
|-----------|-------------|
| <var>a.x, a.y</var> | The first point. |
| <var>b.x, b.y</var> | The second point. |

Calculates the angle between two points.

### Math.clamp(x, min, max)

| Parameter | Description |
|-----------|-------------|
| <var>x</var> | The value to clamp. |
| <var>min</var> | Lowest acceptable value. |
| <var>max</var> | Highest acceptable value. |

Returns the input value clamped between a range [<var>min</var>, <var>max</var>].

### Math.degrees(radians)

| Parameter | Description |
|-----------|-------------|
| <var>radians</var> | The value to convert to degrees. |

Converts radians to degrees.

### Math.distance(a.x, a.y, b.x, b.y)

| Parameter | Description |
|-----------|-------------|
| <var>a.x, a.y</var> | The first point. |
| <var>b.x, b.y</var> | The second point. |

Calculates the distance between two points.

### Math.gravitation(m1, m2, r)

| Parameter | Description |
|-----------|-------------|
| <var>m1</var> | The first mass. |
| <var>m2</var> | The second mass. |
| <var>r</var> | Distance between the centers of the masses. |

Newton's law of universal gravitation: F = G * (m<sub>1</sub> * m<sub>2</sub>) / r<sup>2</sup>.

### Math.hitbox(x, y, width,height, scale)

| Parameter | Description |
|-----------|-------------|
| <var>x, y</var> | Centre of the box. |
| <var>width,height</var> | Dimension of the box. |
| <var>scale</var> | Scaling factor for on-screen box size. Default: 1.0. |

Creates a hitbox.

### Math.is_inside(box, point)

| Parameter | Description |
|-----------|-------------|
| <var>box</var> | Table with the upper-left and lower-right points of the box. |
| <var>point</var> | The point to check. |

Checks whether a point is inside a box.

### Math.radians(degrees)

| Parameter | Description |
|-----------|-------------|
| <var>degrees</var> | The value to convert to radians. |

Converts degrees to radians.

## Parallax

    local Parallax = require(module_path .. "Parallax")

Simple parallax controller.

    local layers = {
      -- A layer is defined by a batch and its velocity on the x- and y-axis
      { batch1, 0.5, 0 }  -- background
      { batch2, 0.8, 0 }  -- midground
      { batch3, 1.0, 0 }  -- foreground
    }
    local pax = Parallax(layers)
    pax:move(0.7 * dt)

Limitations:

  * Does not handle loops.
  * There's a 1:1 mapping between batches and layers even though, logically,
    batches can be on the same layer.

### &lt;Parallax&gt;:hide()

Hides the parallax from view.

### &lt;Parallax&gt;:move(v)

Move layers relatively by <var>v</var> pixels.

### &lt;Parallax&gt;:set_layer_velocity(layer, vx, +vy)

| Parameter | Description |
|-----------|-------------|
| <var>layer</var> | The layer number to change velocity. |
| <var>vx</var> | Relative velocity on the x-axis. |
| <var>vy</var> | <span class="optional">Relative velocity on the y-axis. Default: 0.</span> |

Sets relative layer velocity.

### &lt;Parallax&gt;:set_parent(parent)

Adds parallax to a child node under <var>parent</var>.

### &lt;Parallax&gt;:show()

Unhides the parallax.

## Stack

    local Stack = require(module_path .. "Stack")

Stack is a last in, first out (LIFO) data type and linear data structure. A stack can store any element but only the top element is available at any time.

    local mystack = Stack()
    mystack:push(1)  # Stack is now { 1 }
    mystack:push(2)  # Stack is now { 1, 2 }
    mystack:push(3)  # Stack is now { 1, 2, 3 }
    mystack:push(4)  # Stack is now { 1, 2, 3, 4 }

    local value = mystack:top()  # 'value' is 4

    mystack:pop()          # Stack is now { 1, 2, 3 }
    value = mystack:top()  # 'value' is 3
    mystack:pop()          # Stack is now { 1, 2 }
    value = mystack:top()  # 'value' is 2
    mystack:pop()          # Stack is now { 1 }
    value = mystack:top()  # 'value' is 1
    mystack:pop()          # Stack is now empty

### Stack()

Creates a stack.

### &lt;Stack&gt;:pop()

Pops an element from the top of the stack.

### &lt;Stack&gt;:push(element)

Pushes <var>element</var> on top of the stack.

### &lt;Stack&gt;:top()

Returns the element on top of the stack.

## Timer

    local Timer = require(module_path .. "Timer")

Timers execute a function once after a set time, or any number of times at given intervals.

### Timer(callback, delay, +times)

| Parameter | Description |
|-----------|-------------|
| <var>callback</var> | The function to call on time-out. |
| <var>delay</var> | Time in milliseconds to delay call. |
| <var>times</var> | <span class="optional">Number of times to call. Infinite if omitted.</span> |

Creates a timer.

### Timer.clear()

Clears all timers.

### &lt;Timer&gt;:cancel()

Cancels timer.

### &lt;Timer&gt;:reset(+delay)

| Parameter | Description |
|-----------|-------------|
| <var>delay</var> | <span class="optional">Set new delay.</span>

Resets a timer. Restores it to its initial state.

## Transition

    local Transition = require(module_path .. "Transition")

### Transition.clear()

Cancels all transitions.

### Transition.fadein(source, duration)

| Parameter | Description |
|-----------|-------------|
| <var>source</var> | The audio channel to fade in. |
| <var>duration</var> | Duration of the transition in milliseconds. |

Fades in an audio channel.

### Transition.fadeout(source, duration)

| Parameter | Description |
|-----------|-------------|
| <var>source</var> | The audio channel to fade out. |
| <var>duration</var> | Duration of the transition in milliseconds. |

Fades out an audio channel.

### Transition.fadeto(sprite, alpha, duration, method)

| Parameter | Description |
|-----------|-------------|
| <var>sprite</var> | The sprite to fade. |
| <var>alpha</var> | Final alpha value. |
| <var>duration</var> | Duration of the transition in milliseconds. |
| <var>method</var> | The equation to use for the transition. |

Fades a sprite to a given alpha value.

### Transition.move(drawable, x, y, duration, method)

| Parameter | Description |
|-----------|-------------|
| <var>drawable</var> | A scene graph node or an object that implements ``:move()``. |
| <var>x, y</var> | Number of units to move drawable(s) by, relative to its/their current position(s). |
| <var>duration</var> | Duration of the transition in milliseconds. |
| <var>method</var> | The equation to use for the transition. |

Moves a node (and its children) or a drawable.

### Transition.rotate(drawable, r, duration, method)

| Parameter | Description |
|-----------|-------------|
| <var>drawable</var> | An object that implements ``:get_angle()`` and ``:rotate()``. |
| <var>r</var> | Final angle. |
| <var>duration</var> | Duration of the transition in milliseconds. |
| <var>method</var> | The equation to use for the transition. |

Rotates a drawable to given angle.

### Transition.scaleto(drawable, start, final, duration, method)

| Parameter | Description |
|-----------|-------------|
| <var>drawable</var> | An object that implements ``:set_scale()``. |
| <var>start</var> | Start scale factor. |
| <var>final</var> | Final scale factor. |
| <var>duration</var> | Duration of the transition in milliseconds. |
| <var>method</var> | The equation to use for the transition. |

Scales a drawable from a given start factor to a given final factor.

    </div>
    <footer class="container">
      <hr>
      <p>Copyright &copy; 2010-14 Bifrost Entertainment AS and Tommy Nguyen. Distributed under the <a href="http://opensource.org/licenses/MIT">MIT License</a>.</p>
    </footer>
    <script src="js/vendor/jquery-1.11.0.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/vendor/marked.js"></script>
    <script src="js/vendor/highlight.pack.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>
