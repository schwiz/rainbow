// Copyright (c) 2010-14 Bifrost Entertainment AS and Tommy Nguyen
// Distributed under the MIT License.
// (See accompanying file LICENSE or copy at http://opensource.org/licenses/MIT)

#include <gtest/gtest.h>

#include "Common/Algorithm.test.cpp"
#include "Common/Chrono.test.cpp"
#include "Common/Color.test.cpp"
#include "Common/Data.test.cpp"
#include "Common/SharedPtr.test.cpp"
#include "Common/Vec2.test.cpp"
#include "Common/Vec3.test.cpp"
#include "Common/Vector.test.cpp"
#include "ConFuoco/ConFuoco.test.cpp"
#include "FileSystem/Path.test.cpp"
#include "Input/Touch.test.cpp"
